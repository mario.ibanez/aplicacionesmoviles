(()=>{
const xhr=new XMLHttpRequest();
$xhr= document.getElementById("xhr"),   
$fragment=document.createDocumentFragment();
xhr.addEventListener("readystatechange",(el)=>{
if(xhr.readyState!==4) return;

console.log(xhr);

if(xhr.status>=200  && xhr.status<300){


let json= JSON.parse(xhr.responseText);
console.log(json);

json.forEach((el) => {
    const $li= document.createElement("li");
    $li.innerHTML = `${el.name} -- ${el.email} -- ${el.phone}`;
    $fragment.appendChild($li);
    
});
$xhr.appendChild($fragment);

} else{

    console.log("error");   
}


});
xhr.open("GET","https://jsonplaceholder.typicode.com/users");
xhr.send();
})();




















(()=>{
   
    const $fetch   = document.getElementById("fetch"),   
    $fragment=document.createDocumentFragment();
   fetch("https://jsonplaceholder.typicode.com/users").then((res)=>{
     return  res.json()
   }    


   ).then((json)=>

{
          json.forEach((el) => {
        const $li= document.createElement("li");
        $li.innerHTML = `${el.name} -- ${el.email} -- ${el.phone}`;
        $fragment.appendChild($li);       
}
);
$fetch.appendChild($fragment);
}).catch().finally();
 })();











 (()=>{
   
    const $fetchAsync = document.getElementById("fetch-async"),
    $fragment = document.createDocumentFragment();
  
  async function getData() {
    try {
      let res = await fetch("https://jsonplaceholder.typicode.com/users"),
        json = await res.json();
  
      console.log(res, json);
  
      //if (!res.ok) throw new Error("Ocurrio un Error al solicitar los Datos");
      if (!res.ok) throw { status: res.status, statusText: res.statusText };
  
      json.forEach((el) => {
        const $li = document.createElement("li");
        $li.innerHTML = `${el.name} -- ${el.email} -- ${el.phone}`;
        $fragment.appendChild($li);
      });
  
      $fetchAsync.appendChild($fragment);
    } catch (err) {
      console.log(err);
      let message = err.statusText || "Ocurrió un error";
      $fetchAsync.innerHTML = `Error ${err.status}: ${message}`;
    } finally {
      console.log("Esto se ejecutará independientemente del try... catch");
    }
  }
  
  getData();

})();