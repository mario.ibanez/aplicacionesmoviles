$(document).ready(function() {
    var item, tile, author, publisher, bookLink, bookImg;
    var outputList = document.getElementById("list-output");
    var bookUrl = "https://www.googleapis.com/books/v1/volumes?q=";
    var apiKey = "key=AIzaSyDtXC7kb6a7xKJdm_Le6_BYoY5biz6s8Lw";
    var placeHldr = "https://www.nicepng.com/png/full/777-7771510_closed-book-clipart-book-missing.png";
    var searchData;
    var busqueda;
    var index=0;



    $("#search").click(function() {
        localStorage.setItem("index", 0);
      outputList.innerHTML = ""; //empty html output
      document.body.style.backgroundImage = "url('')";
       searchData = $("#search-box1").val();

       if(searchData === "" || searchData === null) {
         displayError();
       }
      else {
         $.ajax({
            url: bookUrl +"inauthor:" +searchData+"&isbn&filter=ebooks"+"&maxResults=10"+"&startIndex="+index,
            dataType: "json",
            success: function(response) {
              console.log(response)
              if (response.totalItems === 0) {
                alert("Sin Resultados, verifique criterios")
              }
              else {

                displayResults(response);
              }
              localStorage.setItem("busqueda",bookUrl +"inauthor:" +searchData+"&filter=free-ebooks"+"&maxResults=10"+"&startIndex=");
              console.log(busqueda);
            },
            error: function () {
              alert("Algo estaba mal.. </br>"+"Intente de nuevo!");
            }
          });
        }
        $("#search-box1").val(""); //clearn search box
        $("#search-box2").val(""); //clearn search box
    
     });

    $("#searchtitulo").click(function() {
        localStorage.setItem("index", 0);
        outputList.innerHTML = ""; //empty html output
        document.body.style.backgroundImage = "url('')";
         searchData = $("#search-box2").val();
         if(searchData === "" || searchData === null) {
           displayError();
         }
        else {
           $.ajax({
              url: bookUrl +"intitle:"+searchData+"&filter=free-ebooks"+"&maxResults=10"+"&startIndex="+index,
              dataType: "json",
              success: function(response) {
                console.log(response)
                if (response.totalItems === 0) {
                  alert("Sin Resultados, verifique criterios")
                }
                else {
                  displayResults(response);
                }
                localStorage.setItem("busqueda",bookUrl +"intitle:"+searchData+"&filter=free-ebooks"+"&maxResults=10"+"&startIndex=");
                console.log(busqueda);
              },
              error: function () {
                alert("Algo estaba mal.. </br>"+"Intente de nuevo!");
              }
            });
          }
          $("#search-box1").val(""); //clearn search box
          $("#search-box2").val(""); //clearn search box
       });
  














       $("#AvanzaBusqueda").click(function() {
        outputList.innerHTML = ""; //empty html output
        document.body.style.backgroundImage = "url('')";
         searchData = busqueda;
         var dato =parseInt(localStorage.getItem("index"),10);
         console.log(dato);
         localStorage.setItem("index",dato+10);
         console.log(localStorage.getItem("busqueda"))
         if(searchData === "" || searchData === null) {
           displayError();
         }
        else {

           $.ajax({
              url:localStorage.getItem("busqueda")+localStorage.getItem("index"),
              dataType: "json",
              success: function(response) {
                console.log(response)
                if (response.totalItems === 0) {
                  alert("Sin Resultados, verifique criterios")
                }
                else {

                  displayResults(response);
                }
                console.log(busqueda);
              },
              error: function () {
                alert("Algo estaba mal.. </br>"+"Intente de nuevo!");
              }
            });
          }
      
       });











       $("#RetrocedeBusqueda").click(function() {
        outputList.innerHTML = ""; //empty html output
        document.body.style.backgroundImage = "url('')";
         searchData = busqueda;
         var dato =parseInt(localStorage.getItem("index"),10);
         if(dato<10){
             dato=10;
         }
         console.log(dato);
         localStorage.setItem("index",dato-10);
         console.log(localStorage.getItem("busqueda"))
         if(searchData === "" || searchData === null) {
           displayError();
         }
        else {

           $.ajax({
              url:localStorage.getItem("busqueda")+localStorage.getItem("index"),
              dataType: "json",
              success: function(response) {
                console.log(response)
                if (response.totalItems === 0) {
                  alert("Sin Resultados, verifique criterios")
                }
                else {
                  $("#title").animate({'margin-top': '0px'}, 1000); //search box animation
                  $(".book-list").css("visibility", "visible");
                  displayResults(response);
                }
                console.log(busqueda);
              },
              error: function () {
                alert("Algo estaba mal.. </br>"+"Intente de nuevo!");
              }
            });
          }
      
       });















     /*
     * function to display result in index.html
     * @param response
     */
     function displayResults(response) {
        for (var i = 0; i < response.items.length; i++) {
          item = response.items[i];
          title1 = item.volumeInfo.title;
          author1 = item.volumeInfo.authors;
          publisher1 = item.volumeInfo.publisher;
          bookLink1 = item.volumeInfo.previewLink;
          bookIsbn = item.volumeInfo.industryIdentifiers[0].identifier
          disponible =item.volumeInfo.previewLink
          bookImg1 = (item.volumeInfo.imageLinks) ? item.volumeInfo.imageLinks.thumbnail : placeHldr ;
          // in production code, item.text should have the HTML entities escaped.
          
          outputList.innerHTML += '<div id='+i+' class="container">' +
                                  formatOutput(i,bookImg1, title1, author1, publisher1, bookLink1,bookIsbn,disponible) +
                                   '</div>';
             console.log(bookIsbn);
            // document.getElementById(bookIsbn).addEventListener("click",recupero(bookIsbn),false); 
              console.log(bookIsbn);
  
          console.log(outputList);
        }
     }
  
     /*
     * card element formatter using es6 backticks and templates (indivial card)
     * @param bookImg title author publisher bookLink
     * @return htmlCard
     */
     function formatOutput(i,bookImg, title, author, publisher, bookLink,bookIsbn,disponible) {
        //console.log(title + ""+ author +" "+ publisher +" "+ bookLink+" "+ bookImg)
      
      author=String(author[0]);
       var titulocorto = title.substring(0,22);
       author= author.substring(0,18);
       console.log(author);
       var viewUrl = 'book.html?isbn='+bookIsbn; //constructing link for bookviewer
       
          var htmlCard = `
        <h2 class='product' id=${bookIsbn}></h2>
                <img class="img2" id=${bookIsbn} src="${bookImg}" > 
                <button onclick="recupero${i}()">QUITAR</button>
                <button onclick="carrito${i}()">AGREGAR</button>
                 <h5 class="e">${titulocorto}</h5>
                  <p class="">Author: ${author}</p>
                  <a target="_blank" href="${viewUrl}" class="btn btn-secondary">Read Book</a>
                  <a target="_blank" href="${disponible}" class="btn btn-secondary">Ebook</a>
                  
        `  
      console.log(htmlCard)           
       return htmlCard;
       

     }
  
     //handling error for empty search box
     function displayError() {
       alert("No dejes los campos de busqueda vacios!")
     }
  
  });
  





function carrito0(){
    document.getElementById(0).className = "product";
    agregoCarrito(0);

}
function recupero0(){
    document.getElementById(0).className = "container";
    eliminoCarrito(0);
}


function carrito1(){
    document.getElementById(1).className = "product";
    agregoCarrito(1);
}
function recupero1(){
    document.getElementById(1).className = "container";
    eliminoCarrito(1);

}


function carrito2(){
  document.getElementById(2).className = "product";
  agregoCarrito(2);
}
function recupero2(){
  document.getElementById(2).className = "container";
  eliminoCarrito(2);

}

function carrito3(){
  document.getElementById(3).className = "product";
  agregoCarrito(3);
}
function recupero3(){
  document.getElementById(3).className = "container";
  eliminoCarrito(3);

}

function carrito4(){
  document.getElementById(4).className = "product";
  agregoCarrito(4);
}
function recupero4(){
  document.getElementById(4).className = "container";
  eliminoCarrito(4);

}

function carrito5(){
  document.getElementById(5).className = "product";
  agregoCarrito(5);
}
function recupero5(){
  document.getElementById(5).className = "container";
  eliminoCarrito(5);

}



function carrito6(){
  document.getElementById(6).className = "product";
  agregoCarrito(6);
}
function recupero6(){
  document.getElementById(6).className = "container";
  eliminoCarrito(6);

}


function carrito7(){
  document.getElementById(7).className = "product";
  agregoCarrito(7);
}
function recupero7(){
  document.getElementById(7).className = "container";
  eliminoCarrito(7);

}



function carrito8(){
  document.getElementById(8).className = "product";
  agregoCarrito(8);
}
function recupero8(){
  document.getElementById(8).className = "container";
  eliminoCarrito(8);

}

function carrito9(){
  document.getElementById(9).className = "product";
  agregoCarrito(9);
}
function recupero9(){
  document.getElementById(9).className = "container";
  eliminoCarrito(9);

}


function carrito10(){
  document.getElementById(10).className = "product";
  agregoCarrito(10);
}
function recupero10(){
  document.getElementById(10).className = "container";
  eliminoCarrito(10);

}




function agregoCarrito(numero){
    var data=localStorage.getItem("array");
    localStorage.setItem("array",data +"%"+ numero);
};

function eliminoCarrito(numero){
   let data2=String(localStorage.getItem("array"));
   localStorage.removeItem("array");
   const miarray = data2.split("%");
   let dat ="%";
   var i=0;
   console.log(miarray[i]);
   while (i<miarray.length-1){
    console.log(i);
    i=i+1;
     if (miarray[i]==numero){
       miarray[i]="%";
       console.log(i); 
       } else{
          
        if(miarray[i]!=null){
          dat=dat+"%"+miarray[i];
        }
         
       }
   
  
   }

 localStorage.setItem("array",dat);
 console.log(i);
}



function guardar_localstorage(num){
  console.log(num);
  let persona= {
    nombre:"Fernando",
    edad:31,
    correo:num,
    coords:{
      lat:10,
      lng:-10,

    }


  };
  let nombre= num;

  localStorage.setItem("nombre",nombre);
  localStorage.setItem("persona",JSON.stringify(persona));


}

function obtener_localstorage(num){
 let nombre=localStorage.getItem("nombre");
 let persona= JSON.parse(localStorage.getItem("persona"));
 console.log(nombre);
 console.log(persona);

}











