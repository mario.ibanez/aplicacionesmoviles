<html lang="en">
<head>
  <link rel="stylesheet" href="css/estilosajax.css">
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>DOCUMENTO DE TEST</title>
<script src="js/include-html.js"></script>
</head>
<body>
 <div data-include="asserts/header.html"></div>
<main class="container"></main>
<div data-include="asserts/footer.html"></div>
<script>
const d=document,
$main=d.querySelector("main");

const getHTML=(options)=>{
let{url,success,error}=options;
const xhr=new XMLHttpRequest();

xhr.addEventListener("readystatechange",e=>{
if(xhr.readyState!==4) return;
if(xhr.status>=200 && xhr.status<300){
let html=xhr.responseText;
success(html);
}else{
let message=xhr.statusText;
error(`Error ${xhr.status}:${message}`);
}
});



xhr.open("GET",url);
xhr.setRequestHeader("Content-type","text/html;charset=utf-8");
xhr.send();

}





d.addEventListener("DOMContentLoaded",e=>{
getHTML({
url:"asserts/home.html",
success:(html)=>$main.innerHTML=html,
error:(err)=>$main.innerHTML=`<h>${err}</h>`
});
});


d.addEventListener("click",e=>{
if(e.target.matches(".menu a")){
e.preventDefault();
getHTML({
url:e.target.href,
success:(html)=>$main.innerHTML=html,
error:(err)=>$main.innerHTML=`<h>${err}</h>`
});

}});




</script>
</body>
</html>