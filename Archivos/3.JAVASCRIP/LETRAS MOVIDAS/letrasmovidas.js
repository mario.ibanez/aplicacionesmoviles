var time = 0;
var w = window.innerWidth,
    h = window.innerHeight;

for (var i = 0; i < 100; i++) {
  var node = document.createElement('div');
  node.className = 'square';
  node.innerHTML = "MARIO";
  var left = window.innerWidth * Math.random();
  node.style = "top: 30px; left: " + left + "px;";
  document.querySelector('body').appendChild(node);
}
var evolve = function() {
  for (var i = 0; i < 150; i++) {
    $('.square:nth-of-type(' + (i - (-1)) + ')').css({
      'left': w * (0.5 + 0.5 * Math.sin((time + 3 * i) / 25)),
      'top': h * (0.5 + 0.5 * Math.sin((time/3 + 7 * i * i) / 50))
    });
  }
};
var frame = function() {
  time++;
  evolve();
  requestAnimationFrame(frame);
}

frame();